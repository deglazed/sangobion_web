<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Happening_model extends CI_Model {
	
	function __construct() {
		parent::__construct();
	}
	
	function getproduct() {
		$query = $this->db->query('SELECT name FROM my_table LIMIT 1');
		
		$row = $query->row_array();
		echo $row['name'];
	}
	
	function getProductType($productid) {
		$this->db->select('id, title');
		$query = $this->db->get_where('PhotoStack', array('productId' => $productid, 'typeId'=>'1'));
		return $query->result_array();
	}
	
	function add($postdata){
		$data = array(
			'image' => $postdata['upload_data']['file_name'],
			'title'=> $postdata['title'],
			'content' => $postdata['content'],
			'dateEvent'=>$postdata['dateEvent'],
			'productId' => $postdata['productId'],
		);
		
		$this->db->insert('Happening', $data);
	}
	
	function delete($id) {
		$this->db->delete('Happening',array('id'=>$id));
	}
	
	function getcontent($id) {
		$query = $this->db->get_where('Happening', array('id' => $id));
		return $query->row_array();
	}
	
	function update($id, $postdata) {
		$data = array (
				'title' => $postdata['title'],
				'content' => $postdata['content'],
				'productId' => $postdata['productId'],
				'image' => $postdata['image'],
				'dateEvent' => $postdata['dateEvent']
		);
		
		$this->db->where ( 'id', $id );
		$this->db->update ( 'Happening', $data ); 
		
	}
	
	function contentdata($limit=10, $offset=0) {
		$total = $this->db->count_all('Happening');
		$this->db->order_by("id", "desc");
		$this->db->limit($limit,$offset);
		
		$query = $this->db->get('Happening');
		
		return array('data'=>$query->result_array(),'total'=>$total);
	}

}