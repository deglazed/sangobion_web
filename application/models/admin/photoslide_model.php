<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Photoslide_model extends CI_Model {
	
	function __construct() {
		parent::__construct();
	}
	
	function getproduct() {
		$query = $this->db->query('SELECT name FROM my_table LIMIT 1');
		
		$row = $query->row_array();
		echo $row['name'];
	}
	
	function getProductType($productid) {
		$this->db->select('id, title');
		$query = $this->db->get_where('PhotoStack', array('productId' => $productid, 'typeId'=>'1'));
		return $query->result_array();
	}
	
	function add($postdata){
		$data = array(
			'image' => $postdata['upload_data']['file_name'],
			'title'=> $postdata['title'],
			'productId' => $postdata['productId'],
		);
		$this->db->insert('PhotoSlide', $data);
	}
	
	function delete($contentid) {
		$this->db->delete('PhotoSlide',array('id'=>$contentid));
	}
	
	function getcontent($contentid) {
		$query = $this->db->get_where('PhotoSlide', array('id' => $contentid));
		return $query->row_array();
		
	}
	
	function update($id, $postdata) {
		$data = array (
				'title' => $postdata['title'],
				'photoStackId' => $postdata['photoStackId'],
				'image' => $postdata['image'],
		);
		
		$this->db->where ( 'id', $id );
		$this->db->update ( 'PhotoSlide', $postdata ); 
		
	}
	
	function contentdata($limit=10, $offset=0) {
		$total = $this->db->count_all('PhotoSlide');
		$this->db->select('id, title, productId');
		$this->db->order_by("id", "desc");
		$this->db->limit($limit,$offset);
		
		$query = $this->db->get('PhotoSlide');
		
		return array('data'=>$query->result_array(),'total'=>$total);
	}

}