<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Photostack_model extends CI_Model {
	
	function __construct() {
		parent::__construct();
	}
	
	function getproduct() {
		$query = $this->db->query('SELECT name FROM my_table LIMIT 1');
		
		$row = $query->row_array();
		echo $row['name'];
	}
	
	function getProductType($productid) {
		$this->db->select('id, title');
		$query = $this->db->get_where('PhotoStack', array('productId' => $productid, 'typeId'=>'1'));
		return $query->result_array();
	}
	
	function add($postdata){
		$data = array(
			'image' => $postdata['upload_data']['file_name'],
			'title'=> $postdata['title'],
			'content' => $postdata['content'],
			'typeId' => 1,
			'productId' => $postdata['productId'],
		);
		$this->db->insert('PhotoStack', $data);
	}
	
	function delete($contentid) {
		$this->db->delete('PhotoStack',array('id'=>$contentid));
	}
	
	function getcontent($contentid) {
		$query = $this->db->get_where('PhotoStack', array('id' => $contentid));
		
		return $query->row_array();
		
	}
	
	function update($id, $postdata) {
		$data = array (
				'title' => $postdata['title'],
				'content' => $postdata['content'],
				'productId' => $postdata['productId'],
				'image' => $postdata['image'],
				'typeId'=>1,
		);
		
		$this->db->where ( 'id', $id );
		$this->db->update ( 'PhotoStack', $data ); 
		
	}
	
	function contentdata($limit=10, $offset=0) {
		$query = $this->db->get_where('PhotoStack', array('typeId'=>'1'));
		$total = $query->num_rows();
		
		$this->db->select('id, title, content,productId');
		$this->db->order_by("id", "desc");
		$this->db->limit($limit,$offset);
		
		$query = $this->db->get_where('PhotoStack', array('typeId'=>'1'));
		
		return array('data'=>$query->result_array(),'total'=>$total);
	}

}