<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Content_model extends CI_Model {
	
	function __construct() {
		parent::__construct();
	}
	
	function getproduct() {
		$query = $this->db->query('SELECT name FROM my_table LIMIT 1');
		
		$row = $query->row_array();
		echo $row['name'];
	}
	
	function getProductType($productid) {
		$this->db->select('id, title');
		$query = $this->db->get_where('PhotoStack', array('productId' => $productid, 'typeId'=>'1'));
		return $query->result_array();
	}
	
	function add($postdata){
		$data = array(
			'image' => $postdata['upload_data']['file_name'],
			'title'=> $postdata['title'],
			'content' => $postdata['content'],
			'photoStackId' => $postdata['photoStackId'],
		);
		$this->db->insert('PhotoStackList', $data);
	}
	
	function delete($contentid) {
		$this->db->delete('PhotoStackList',array('id'=>$contentid));
	}
	
	function getcontent($contentid) {
		$this->db->select('PhotoStackList.id as photostacklistid, PhotoStackList.title as photostacklist, PhotoStackList.content,
				PhotoStackList.image, PhotoStack.id as photostackid, PhotoStack.title as photostacktitle, PhotoStack.typeId, PhotoStack.productId');
		$this->db->join('PhotoStack','PhotoStackList.photoStackId = PhotoStack.id');
		$query = $this->db->get_where('PhotoStackList', array('PhotoStackList.id' => $contentid));
		
		return $query->row_array();
		
	}
	
	function update($contentid, $postdata) {
		$data = array (
				'title' => $postdata['title'],
				'content' => $postdata['content'],
				'photoStackId' => $postdata['photoStackId'],
				'image' => $postdata['image'],
		);
		
		$this->db->where ( 'id', $contentid );
		$this->db->update ( 'PhotoStackList', $data ); 
		
	}
	
	function contentdata($limit=10, $offset=0) {
		$total = $this->db->count_all('PhotoStackList');
		
		$this->db->select("PhotoStackList.id, PhotoStackList.title, SUBSTRING_INDEX(PhotoStackList.content,' ',5) as content, PhotoStack.title as product
				, PhotoStack.productId", FALSE);
		$this->db->from('PhotoStackList');
		$this->db->join('PhotoStack', 'PhotoStackList.photoStackId = PhotoStack.id');
		$this->db->order_by("PhotoStackList.id", "desc");
		$this->db->limit($limit,$offset);
		
		$query = $this->db->get();
		
		return array('data'=>$query->result_array(),'total'=>$total);
	}

}