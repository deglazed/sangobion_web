<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Photoslide extends CI_Controller {

	function __construct() {
		parent::__construct();
		
		if( $this->session->userdata('logged_in') == FALSE) redirect('admin/user/login');
		
		$this->load->model('admin/photoslide_model'); // load model
		
		$this->layout->setApp('admin');
		$this->data = null;
		
		
	}
		
	public function list_all($offset=0) {
		$this->load->library('pagination');
		$this->data['num']  = (max($offset-1,0))*10+1;
		$contentdata = $this->photoslide_model->contentdata(10,(max($offset-1,0))*10);
		$this->data['current_page'] = $offset ? $offset : 1;
		$this->data['listcontent'] = $contentdata['data'];
		$config['base_url'] = base_url().'admin/content/list_all';
		$config['total_rows'] = $contentdata['total'];
		$this->data['totalpage'] = ceil($contentdata['total'] / 10);
		$this->data['totalcontent'] = $contentdata['total'];
		
		$this->layout->view('photoslide_list', $this->data);
	}
	
	public function edit($id) {
		$data = $this->photoslide_model->getcontent($id);
		$this->data['photoslide'] = $data;
	
		$postdata = $this->input->post(NULL,TRUE);
	
		if( $postdata ) {
			$uploadpath = $this->config->item('article_upload_path').'photoslide/';
			$config['upload_path'] = $uploadpath;
			$config['allowed_types'] = '*';
			$config['encrypt_name'] = TRUE;
			$this->load->library('upload', $config);
				
			$data = array(
					'title' =>$postdata['title'],
					'productId'=> $postdata['productid'],
					'image' => $data['image'],
			);
				
			if (!$_FILES['image1']['error']==4){
				if( $this->upload->do_upload('image1')) {
					$upload_data = $this->upload->data();
					unlink($uploadpath.$data['image']);
					$data['image'] = $upload_data['file_name'];
				} else {
					$this->data['error_file_upload'] = $this->upload->display_errors();
				}
			}
				
			if(!$this->upload->display_errors()) {
				$this->photoslide_model->update($id,$data);
				redirect('/admin/photoslide/list_all');
			}
		}
	
		$this->layout->view('photoslide_edit', $this->data);
	}
	
	public function delete($contentid) {
		$content = $this->photoslide_model->getcontent($contentid);
		$uploadpath = $this->config->item('article_upload_path').'photoslide/';
		$file = $uploadpath.$content['image'];
		$this->photoslide_model->delete($contentid);
		unlink($uploadpath.$content['image']);
		redirect('/admin/photoslide/list_all');
	}
	
	public function add() {
		
		$postdata = $this->input->post(NULL,TRUE);
		
		if( $postdata ) {
			$config['upload_path'] = $this->config->item('article_upload_path').'photoslide/';
			$config['allowed_types'] = '*';
			$config['encrypt_name'] = TRUE;
			$this->load->library('upload', $config);
			
			if( $this->upload->do_upload('image1')) {
				$data = array(
					'upload_data'=>$this->upload->data(),
					'title' =>$postdata['title'],
					'productId'=> $postdata['productid'],
				);
				$this->photoslide_model->add($data);
				redirect('admin/photoslide/list_all');
			} else {
				$this->data['error_file_upload'] = $this->upload->display_errors();
			}
		}
		
		$this->layout->view('photoslide_add', $this->data);
	}
	
	function getProductType($productid) {
		$product = $this->content_model->getProductType($productid);
		$product_type = array();
		foreach ($product as $row) {
			$product_data[$row['id']] = strip_tags($row['title']);
		}
		echo json_encode($product_data);
	}
	
	
	
	private function jsplugin() {
		return "";
	}
}

/* End of file photoslide.php */
/* Location: ./application/admin/photoslide.php */