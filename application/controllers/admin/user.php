<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User extends CI_Controller {

	function __construct() {
		parent::__construct();
		$this->data = null;
	}
		
	function login() {
		if( $this->session->userdata('logged_in') == TRUE ) redirect('admin/content/list_all');
		if ( $post = $this->input->post(NULL,TRUE) ) {
			if ($post['username'] == 'admin' && $post['password'] == 'sangobionadmin' ) {
				$this->session->set_userdata(array('logged_in'=>TRUE));
				redirect('admin/content/list_all');
			} else {
				$this->data['error'] = array('username_password' => 'Invalid Username and Password');
			}
		}
		
		$this->load->view( 'admin/user_login',  $this->data);
	}
	
	function logout() {
		$this->session->unset_userdata( array('logged_in'=>'') );
		redirect('admin/user/login');
	}
}

/* End of file user.php */
/* Location: ./application/admin/user.php */