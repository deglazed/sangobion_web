<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Content extends CI_Controller {

	function __construct() {
		parent::__construct();
		if( $this->session->userdata('logged_in') == FALSE) redirect('admin/user/login');
		
		$this->load->model('admin/content_model'); // load model
		
		$this->layout->setApp('admin');
		$this->data = null;
		
		$this->data['jsplugin'] = $this->jsplugin();
		
	}
		
	public function list_all($offset=0) {
		$this->load->library('pagination');
		$this->data['num']  = (max($offset-1,0))*10+1;
		$contentdata = $this->content_model->contentdata(10,(max($offset-1,0))*10);
		$this->data['current_page'] = $offset?$offset:1;
		$this->data['listcontent'] = $contentdata['data'];
		
		$this->data['totalpage'] = ceil($contentdata['total'] / 10);
		$this->data['totalcontent'] = $contentdata['total'];
		
		$this->layout->view('content_list', $this->data);
	}
	
	public function edit($contentid) {
		$content_data = $this->content_model->getcontent($contentid);
		$this->data['contentdata'] = $content_data;
		
		$this->data['product_type'] = $this->content_model->getProductType($content_data['productId']);
		
		$postdata = $this->input->post(NULL,TRUE);
		
		if( $postdata ) {
			$uploadpath = $this->config->item('article_upload_path');
			$config['upload_path'] = $uploadpath;
			$config['allowed_types'] = '*';
			$config['encrypt_name'] = TRUE;
			$this->load->library('upload', $config);
			
			$data = array(
				'title' =>$postdata['title'],
				'content' =>$postdata['content'],
				'photoStackId'=> $postdata['photostackid'],
				'image' => $content_data['image'],
			);
			
			if (!$_FILES['image1']['error']==4){	
				if( $this->upload->do_upload('image1')) {
					$upload_data = $this->upload->data();
					unlink($uploadpath.$content_data['image']);
					$data['image'] = $upload_data['file_name'];
					
				} else {
					$this->data['error_file_upload'] = $this->upload->display_errors();
				}
			}
			
			if(!$this->upload->display_errors()) {
				$this->content_model->update($contentid,$data);
				redirect('/admin/content/list_all');
			}
		}
		
		$this->layout->view('content_edit', $this->data);
	}
	
	public function delete($contentid) {
		$content = $this->content_model->getcontent($contentid);
		$uploadpath = $this->config->item('article_upload_path');
		$file = $uploadpath.$content['image'];
		$this->content_model->delete($contentid);
		unlink($uploadpath.$content['image']);
		redirect('/admin/content/list_all');
	}
	
	public function add() {
		
		$postdata = $this->input->post(NULL,TRUE);
		
		$this->data['product_type'] = $this->content_model->getProductType(1);
		
		if( $postdata ) {
			$config['upload_path'] = $this->config->item('article_upload_path');
			$config['allowed_types'] = '*';
			$config['encrypt_name'] = TRUE;
			$this->load->library('upload', $config);
			
			if( $this->upload->do_upload('image1')) {
				$data = array(
					'upload_data'=>$this->upload->data(),
					'title' =>$postdata['title'],
					'content' =>$postdata['content'],
					'photoStackId'=> $postdata['photostackid'],
				);
				$this->content_model->add($data);
				redirect('admin/content/list_all');
			} else {
				$this->data['error_file_upload'] = $this->upload->display_errors();
			}
		}
		
		$this->layout->view('content_add', $this->data);
	}
	
	function getProductType($productid) {
		$product = $this->content_model->getProductType($productid);
		$product_type = array();
		foreach ($product as $row) {
			$product_data[$row['id']] = strip_tags($row['title']);
		}
		echo json_encode($product_data);
	}
	
	
	
	private function jsplugin() {
		return "";
	}
}

/* End of file content.php */
/* Location: ./application/admin/content.php */