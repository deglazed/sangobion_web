<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN PAGE HEADER-->
			<div class="row">
				<div class="col-md-12">
					<!-- BEGIN PAGE TITLE & BREADCRUMB-->
					<h3 class="page-title">
					Edit Content <small>Edit content</small>
					</h3>
					<ul class="page-breadcrumb breadcrumb">
						<li>
							<i class="fa fa-home"></i>
							<a href="<?php echo base_url();?>admin/">
								Home
							</a>
							<i class="fa fa-angle-right"></i>
						</li>
						<li>
							<a href="<?php echo base_url()?>admin/content/list_all">
								Content
							</a>
							<i class="fa fa-angle-right"></i>
						</li>
						<li>
							<a href="<?php echo base_url()?>admin/content/edit/<?php echo $contentdata['photostacklistid'];?>">
								Edit
							</a>
							
						</li>
					</ul>
					<!-- END PAGE TITLE & BREADCRUMB-->
				</div>
			</div>
			<!-- END PAGE HEADER-->
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12 ">
					<!-- BEGIN SAMPLE FORM PORTLET-->
					<div class="portlet box blue">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-reorder"></i> Edit Content
							</div>
							<div class="tools">
								<a href="" class="collapse">
								</a>
								<a href="#portlet-config" data-toggle="modal" class="config">
								</a>
								<a href="" class="reload">
								</a>
								<a href="" class="remove">
								</a>
							</div>
						</div>
						<div class="portlet-body form">
							<form action="<?php echo base_url();?>admin/content/edit/<?php echo $contentdata['photostacklistid']; ?>" class="form-horizontal form-bordered" method="post" enctype="multipart/form-data">
								<div class="form-body">
									<div class="form-group">
										<label class="col-md-3 control-label">Title</label>
										<div class="col-md-9">
											<input type="text" name="title" value="<?php echo $contentdata['photostacklist']?>" class="form-control" placeholder="Enter text">
											<span class="help-block"></span>
										</div>
									</div>
									<div class="form-group">
										<label class="control-label col-md-3">Product</label>
										<div class="col-md-9" >
											<select class="form-control product-dropdown" name="Product">
												<option value="1" <?php if($contentdata['productId']==1)echo 'selected="selected"'; ?>>Femine</option>
												<option value="2" <?php if($contentdata['productId']==2)echo 'selected="selected"'; ?>>Baby</option>
												<option value="3" <?php if($contentdata['productId']==3)echo 'selected="selected"'; ?>>Capsule</option>
												<option value="4" <?php if($contentdata['productId']==4)echo 'selected="selected"'; ?>>Kids</option>
											</select>
										</div>
									</div>
									<div class="form-group">
										<label class="control-label col-md-3">Type</label>
										<div class="col-md-9" >
											<select class="form-control product-type-dropdown" name="photostackid">
												<?php foreach ($product_type as $row):?>
												<option value="<?php echo $row['id']?>" <?php if($row['id']==$contentdata['photostackid']) echo 'selected="selected"' ;?>><?php echo $row['title']?></option>
												<?php endforeach;?>
											</select>
										</div>
									</div>
									<div class="form-group ">
										
										<label class="control-label col-md-3">Upload Image</label>
										<div class="col-md-9">
											<div class="fileinput fileinput-new" data-provides="fileinput">
												<div class="fileinput-preview thumbnail" data-trigger="fileinput" >
												<img src="<?php echo $this->config->item('article_images').$contentdata['image'];?>">
												</div>
												<div>
													<span class="btn default btn-file">
														<span class="fileinput-new">
															 Select image
														</span>
														<span class="fileinput-exists">
															 Change
														</span>
														<input type="file" name="image1" >
														
													</span>
													<a href="#" class="btn default fileinput-exists" data-dismiss="fileinput">
														 Remove
													</a>
												</div>
												
											</div>
										</div>
										
									</div>
									<div class="form-group last">
										<label class="control-label col-md-3">Content</label>
										<div class="col-md-9">
											<textarea class="ckeditor form-control" name="content" rows="6"><?php echo $contentdata['content']?></textarea>
										</div>
									</div>
								</div>
								<div class="form-actions fluid">
									<div class="row">
										<div class="col-md-12">
											<div class="col-md-offset-3 col-md-9">
												<button type="submit" name="submit" class="btn purple"><i class="fa fa-check"></i> Submit</button>
												<button type="button" class="btn default" onclick="history.go(-1);return false;">Cancel</button>
											</div>
										</div>
									</div>
								</div>
							</form>
						</div>
						
					</div>
					<!-- END SAMPLE FORM PORTLET-->
					
				</div>
			</div>
			
			<!-- END PAGE CONTENT-->
		</div>
	</div>