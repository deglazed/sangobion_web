<!DOCTYPE html>
<!-- 
Template Name: Metronic - Responsive Admin Dashboard Template build with Twitter Bootstrap 3.1.1
Version: 2.0.2
Author: KeenThemes
Website: http://www.keenthemes.com/
Contact: support@keenthemes.com
Purchase: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
License: You must have a valid license purchased only from themeforest(the above link) in order to legally use the theme for your project.
-->
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" class="no-js">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
<meta charset="utf-8"/>
<title>Metronic | Admin Dashboard Template</title>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<meta content="" name="description"/>
<meta content="" name="author"/>
<!-- BEGIN GLOBAL MANDATORY STYLES -->
<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css"/>
<link href="<?php echo asset_url(); ?>plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo asset_url(); ?>plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo asset_url(); ?>plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css"/>
<!-- END GLOBAL MANDATORY STYLES -->
<!-- BEGIN PAGE LEVEL PLUGIN STYLES -->
<link rel="stylesheet" type="text/css" href="<?php echo asset_url(); ?>plugins/bootstrap-fileinput/bootstrap-fileinput.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo asset_url(); ?>plugins/bootstrap-switch/css/bootstrap-switch.min.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo asset_url(); ?>plugins/jquery-tags-input/jquery.tagsinput.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo asset_url(); ?>plugins/bootstrap-markdown/css/bootstrap-markdown.min.css">
<link rel="stylesheet" type="text/css" href="<?php echo asset_url(); ?>plugins/typeahead/typeahead.css">
<link href="<?php echo asset_url(); ?>plugins/jquery-easy-pie-chart/jquery.easy-pie-chart.css" rel="stylesheet" type="text/css"/>

<link rel="stylesheet" type="text/css" href="<?php echo asset_url();?>plugins/clockface/css/clockface.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo asset_url();?>plugins/bootstrap-datepicker/css/datepicker.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo asset_url();?>plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo asset_url();?>plugins/bootstrap-colorpicker/css/colorpicker.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo asset_url();?>plugins/bootstrap-daterangepicker/daterangepicker-bs3.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo asset_url();?>plugins/bootstrap-datetimepicker/css/datetimepicker.css"/>


<!-- END PAGE LEVEL PLUGIN STYLES -->
<!-- BEGIN THEME STYLES -->
<link href="<?php echo asset_url(); ?>css/style-metronic.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo asset_url(); ?>css/style.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo asset_url(); ?>css/style-responsive.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo asset_url(); ?>css/plugins.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo asset_url(); ?>css/pages/tasks.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo asset_url(); ?>css/themes/default.css" rel="stylesheet" type="text/css" id="style_color"/>
<link href="<?php echo asset_url(); ?>css/print.css" rel="stylesheet" type="text/css" media="print"/>
<link href="<?php echo asset_url(); ?>css/custom.css" rel="stylesheet" type="text/css"/>
<!-- END THEME STYLES -->
<link rel="shortcut icon" href="favicon.ico"/>
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="page-header-fixed">
<!-- BEGIN HEADER -->
<div class="header navbar navbar-fixed-top">
	<?php echo $header;?>
</div>
<!-- END HEADER -->
<div class="clearfix">
</div>
<!-- BEGIN CONTAINER -->
<div class="page-container">
	<!-- BEGIN SIDEBAR -->
	<?php echo $sidebar; ?>
	<!-- END SIDEBAR -->
	<!-- BEGIN CONTENT -->
	<?php echo $content;?>
	<!-- END CONTENT -->
</div>
<!-- END CONTAINER -->
<!-- BEGIN FOOTER -->
<div class="footer">
	<div class="footer-inner">
		 2014 &copy; Sangobion.
	</div>
	<div class="footer-tools">
		<span class="go-top">
			<i class="fa fa-angle-up"></i>
		</span>
	</div>
</div>
<!-- END FOOTER -->
<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<!-- BEGIN CORE PLUGINS -->
<!--[if lt IE 9]>
<script src="<?php echo asset_url(); ?>plugins/respond.min.js"></script>
<script src="<?php echo asset_url(); ?>plugins/excanvas.min.js"></script> 
<![endif]-->
<script src="<?php echo asset_url(); ?>plugins/jquery-1.10.2.min.js" type="text/javascript"></script>
<script src="<?php echo asset_url(); ?>plugins/jquery-migrate-1.2.1.min.js" type="text/javascript"></script>
<!-- IMPORTANT! Load jquery-ui-1.10.3.custom.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
<script src="<?php echo asset_url(); ?>plugins/jquery-ui/jquery-ui-1.10.3.custom.min.js" type="text/javascript"></script>
<script src="<?php echo asset_url(); ?>plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="<?php echo asset_url(); ?>plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
<script src="<?php echo asset_url(); ?>plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
<script src="<?php echo asset_url(); ?>plugins/jquery.blockui.min.js" type="text/javascript"></script>
<script src="<?php echo asset_url(); ?>plugins/jquery.cokie.min.js" type="text/javascript"></script>
<script src="<?php echo asset_url(); ?>plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
<!-- END CORE PLUGINS -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<!-- IMPORTANT! fullcalendar depends on jquery-ui-1.10.3.custom.min.js for drag & drop support -->
<script src="<?php echo asset_url(); ?>plugins/jquery-easy-pie-chart/jquery.easy-pie-chart.js" type="text/javascript"></script>
<script src="<?php echo asset_url(); ?>plugins/jquery.sparkline.min.js" type="text/javascript"></script>
<script type="text/javascript" src="<?php echo asset_url(); ?>plugins/bootstrap-fileinput/bootstrap-fileinput.js"></script>
<script type="text/javascript" src="<?php echo asset_url(); ?>plugins/ckeditor/ckeditor.js"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="<?php echo asset_url(); ?>scripts/core/app.js" type="text/javascript"></script>
<script src="<?php echo asset_url(); ?>scripts/custom/index.js" type="text/javascript"></script>
<script src="<?php echo asset_url(); ?>scripts/custom/tasks.js" type="text/javascript"></script>
<script src="<?php echo asset_url(); ?>scripts/custom/components-editors.js"></script>
<script src="<?php echo asset_url();?>scripts/custom/components-pickers.js"></script>

<script type="text/javascript" src="<?php echo asset_url();?>plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="<?php echo asset_url();?>plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js"></script>
<script type="text/javascript" src="<?php echo asset_url();?>plugins/clockface/js/clockface.js"></script>
<script type="text/javascript" src="<?php echo asset_url();?>plugins/bootstrap-daterangepicker/moment.min.js"></script>
<script type="text/javascript" src="<?php echo asset_url();?>plugins/bootstrap-daterangepicker/daterangepicker.js"></script>
<script type="text/javascript" src="<?php echo asset_url();?>plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>
<script type="text/javascript" src="<?php echo asset_url();?>plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js"></script>
<!-- END PAGE LEVEL SCRIPTS -->
<script>
jQuery(document).ready(function() {    
   App.init(); // initlayout and core plugins
   Index.init();
   Index.initMiniCharts();
   Index.initIntro();
   ComponentsEditors.init();
   ComponentsPickers.init();
   
   $(".product-dropdown").change(function() {
	   $('.product-type-dropdown').empty();
	   $.get("<?php echo base_url()?>admin/content/getProductType/"+$(this).val(),function(data,status){
		   $.each(JSON.parse(data), function(key, value) {   
			     $('.product-type-dropdown')
			         .append($("<option></option>")
			         .attr("value",key)
			         .text(value)); 
			});
		});
	});

 	$(".delete-item").click(function(){
	   var title = $(this).parent().parent().children().next().html();
	   var id = $(this).siblings().val();
	   //var id = $(this).sib 
	   $("h4.modal-title").html(title);
	   $(".confirm-delete").click(function(){
           window.location.href = "<?php echo base_url()?>admin/<?php echo $this->router->class?>/delete/"+id;
	   });
	});

	
});
</script>
<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>