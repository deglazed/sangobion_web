<div class="page-sidebar-wrapper">
		<div class="page-sidebar navbar-collapse collapse">
			<!-- add "navbar-no-scroll" class to disable the scrolling of the sidebar menu -->
			<!-- BEGIN SIDEBAR MENU -->
			<ul class="page-sidebar-menu" data-auto-scroll="true" data-slide-speed="200">
				<li class="start active ">
					<a href="javascript:;">
						<i class="fa fa-bookmark-o"></i>
						<span class="title">
							Dashboard
						</span>
						<span class="arrow ">
						</span>
					</a>
					<ul class="sub-menu">
						<li <?php if ($this->router->class == 'happening'):?>class="active" <?php endif; ?> >
							<a href="<?php echo base_url();?>admin/happening/list_all">
								Happening
							</a>
						</li>
						<li <?php if ($this->router->class == 'photoslide'):?>class="active" <?php endif; ?> >
							<a href="<?php echo base_url();?>admin/photoslide/list_all">
								Photoslide
							</a>
						</li>
						<li <?php if ($this->router->class == 'photostack'):?>class="active" <?php endif; ?> >
							<a href="<?php echo base_url();?>admin/photostack/list_all">
								Photostack
							</a>
						</li>
						<li <?php if ($this->router->class == 'content'):?>class="active" <?php endif; ?> >
							<a href="<?php echo base_url();?>admin/content/list_all">
								Photostack list
							</a>
						</li>
					</ul>
				</li>
			</ul>
			<!-- END SIDEBAR MENU -->
		</div>
	</div>