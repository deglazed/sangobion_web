<div class="page-content-wrapper">
	<div class="page-content">
		<!-- BEGIN PAGE HEADER-->
		<div class="row">
			<div class="col-md-12">
				<!-- BEGIN PAGE TITLE & BREADCRUMB-->
				<h3 class="page-title">
					Photoslide <small>List All Photoslide</small>
				</h3>
				<ul class="page-breadcrumb breadcrumb">
					<li><i class="fa fa-home"></i> <a
						href="<?php echo base_url();?>admin/"> Home </a> <i
						class="fa fa-angle-right"></i></li>
					<li><a href="<?php echo base_url()?>admin/photoslide/list_all">
							Photoslide </a></li>
				</ul>
				<!-- END PAGE TITLE & BREADCRUMB-->
			</div>
		</div>
		<!-- END PAGE HEADER-->
		<!-- BEGIN PAGE CONTENT-->
		<div class="row">
			<div class="col-md-12">
				<!-- BEGIN SAMPLE TABLE PORTLET-->
				<div class="portlet box blue">
					<div class="portlet-title">
						<div class="caption">All Photoslide</div>
						<div class="tools">
							<a href="javascript:;" class="collapse"> </a> <a
								href="#portlet-config" data-toggle="modal" class="config"> </a>
							<a href="javascript:;" class="reload"> </a> <a
								href="javascript:;" class="remove"> </a>
						</div>
					</div>
					<div class="portlet-body">
						<div class="table-toolbar">
							<div class="btn-group">
								<button id="sample_editable_1_new" class="btn green" onclick="location.href='<?php echo base_url();?>admin/photoslide/add'">
									Add New <i class="fa fa-plus"></i>
								</button>
							</div>

						</div>
						<div class="table-responsive">
							<table class="table table-striped table-bordered table-hover">
								<thead>
									<tr>
										<th>#</th>
										<th>Title</th>
										<th>Product</th>
										<th></th>
										<th></th>
									</tr>
								</thead>
								<tbody>
								<?php $i=$num?>
								<?php foreach ($listcontent as $row):?>
								<tr>
										<td>
										<?php echo $i++?>
									</td>
										<td>
										 <?php echo $row['title']?>
									</td>
										<td>
										<?php $product = array(1=>'femine', 2=>'baby', 3=>'capsule', 4=>'kids');?>
										<?php echo $product[$row['productId']]?>
									</td>
									<td>
										<input type="hidden" value="<?php echo $row['id']?>" />
										<a
											data-toggle="modal" href="#basic"
											class="btn default btn-xs black delete-item"> <i class="fa fa-trash-o"></i>
												Delete
										</a>
									</td>
									<td>
										<a
											href="<?php echo base_url()?>admin/photoslide/edit/<?php echo $row['id']?>"
											class="btn default btn-xs purple"> <i class="fa fa-edit"></i>
												Edit
										</a>
									</td>
								</tr>
								<?php endforeach;?>
								</tbody>
							</table>
						</div>
						<div class="row">
							<div class="col-md-5 col-sm-12">
								<div class="dataTables_info" id="sample_editable_1_info">Showing
									<?php echo $num;?> to <?php echo $i-1?> of <?php echo $totalcontent?> entries</div>
							</div>
							<div class="col-md-7 col-sm-12">
								<div class="dataTables_paginate paging_bootstrap">
									<ul class="pagination" style="visibility: visible;">
										<li
											class="prev <?php echo ($current_page-1==0)?'disabled':$current_page-1;?>">
											<?php if($current_page-1 != 0): ?>
											<a
											href="<?php echo base_url()?>admin/photoslide/list_all/<?php echo ($current_page-1==0)? '' : $current_page-1;?>"
											title="Prev"> <i class="fa fa-angle-left"></i></a>
											<?php else:?>
											<a><i class="fa fa-angle-left"></i></a>
											<?php endif;?>
										</li>
										<?php for ($i = 0;$i < $totalpage;$i++):?>
										<li <?php echo ($current_page-1==$i)?'class="active"':''?>><a
											href="<?php echo base_url()?>admin/photoslide/list_all/<?php echo $i+1?>"><?php echo $i+1?></a></li>
										<?php endfor;?>
										
										
										<li
											class="next <?php if($current_page == $totalpage){?>disabled<?php }?>"><a
											<?php if($current_page+1 <= $totalpage){?>
											href="<?php echo base_url()?>admin/photoslide/list_all/<?php echo $current_page+1;?>"
											<?php }?> title="Next"><i class="fa fa-angle-right"></i></a></li>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- END SAMPLE TABLE PORTLET-->
			</div>
		</div>
		<!-- END PAGE CONTENT-->
	</div>
</div>

<div class="modal fade" id="basic" tabindex="-1" role="basic" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content ">
				<div class="modal-header box-red">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
					<h4 class="modal-title">Modal Title</h4>
				</div>
				<div class="modal-body">
					 Are you sure you want to delete this?
				</div>
				<div class="modal-footer">
					<button type="button" class="btn default" data-dismiss="modal">Cancle</button>
					<button type="button" class="btn red confirm-delete">Delete</button>
				</div>
			</div>
			<!-- /.modal-content -->
		</div>
		<!-- /.modal-dialog -->
	</div>
